let express =require('express'); let router = express.Router();
let HomeImages = require('../../database/mainImages');

router.get('/', (req, res)=>{
    HomeImages.getActive(null, (err, data)=>{
       res.send({code : 200, data : {images : data}});
    });
});

module.exports = router;