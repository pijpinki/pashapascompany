let express  = require('express');
let router   = express.Router();
let Products = require('../../database/products');

router.get('/', (req, res)=>{
    Products.getAll((err, data)=>{
       res.send({code :200, data : {products:data}});
    });
});

module.exports = router;