let mysql       = require('./connect');
let ProductInfo = require('./product_info');
let Asycn       = require("async");
module.exports.getAll = function (callback) {
    let out = [];

    function getMain(_callback) {
        let sql = "SELECT * FROM products ";
        mysql.query(sql, (err, data)=>{
            if(err) throw err;
            data.length > 0 ? _callback(null, data) : _callback(true, data);
        });
    }

    function getDescription(product, _callback) {
        ProductInfo.getInfo({product_id: product.id}, (err, data) => {
            product.description = {};
            if(data.length > 0){
               product.description.ru = data[0].description_ru;
               product.description.en = data[0].description_en;
            }
            out.push(product);
            _callback(null,product);
        });
    }

    getMain((err,data)=>{
        if(err) callback(null,[]);
        else {
            Asycn.map(data, getDescription, (err,data)=>{
               callback(null, out);
            });
        }
    })


};