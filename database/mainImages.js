let mysql = require("./connect");

module.exports.getAll = function (params, callback) {
    let sql = "SELECT * FROM home_images";
    mysql.query(sql, (err, data) => {
        if(err) throw err;
        callback(err, data);
    });
};

module.exports.getActive = function (params, callback) {
    let sql = "SELECT * FROM home_images WHERE active = 1";
    mysql.query(sql, (err, data) => {
        if(err) throw err;
        callback(err, data);
    });
};