let mysql = require("./connect");

/**
 *
 * @param params
 * @param params.product_id
 * @param callback
 */
module.exports.getInfo = function (params, callback) {
    let sql = "SELECT * FROM product_info WHERE product_id = ?";
    mysql.query(sql, [params.product_id], (err,data)=>{
        if(err) throw err;
        callback(null, data);
    });
};