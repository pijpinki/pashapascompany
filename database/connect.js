let mysql  = require('mysql');
let config = require('../config');

module.exports = mysql.createPool(config.database);