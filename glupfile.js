var gulp = require("gulp");
var sourcemaps = require("gulp-sourcemaps");
var babel = require("gulp-babel");
// var concat = require("gulp-concat");

gulp.task("main", function () {
    return gulp.src("./public/js/react/src/main/*.js")
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("./public/js/react"));
});


gulp.task("components", function () {
    return gulp.src("./public/js/react/src/components/*.js")
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("./public/js/react/components"));
});

