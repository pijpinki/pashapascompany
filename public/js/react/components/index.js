class IndexMain extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            images: null,
            title: null,
            text: null
        };

        this.main = p.main;
    }

    componentWillMount() {
        this.getImages();
        this.getText();
    }

    componentWillReceiveProps() {
        this.getImages();
        this.getText();
    }

    getImages() {
        this.setState({ images: React.createElement(IndexImages, null) }, () => {});
    }

    getText() {
        this.setState({ title: Strings[Strings.current].index.body.title, text: this.generateDesc.call(this) }, () => {});
    }

    generateDesc() {
        return Strings[Strings.current].index.body.description.map((text, index) => this.desc(text, index));
    }

    desc(text, i) {
        return React.createElement(
            "p",
            { className: "text-center lead", key: i },
            text
        );
    }

    render() {
        return React.createElement(
            "div",
            null,
            this.state.images,
            React.createElement(
                "div",
                { style: { marginTop: "100vh" } },
                React.createElement(
                    "h1",
                    { className: "text-center index-title display-2" },
                    this.state.title
                ),
                this.state.text
            )
        );
    }
}

class IndexImages extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            images: [],
            strings: {},

            main: null,
            indicators: null
        };

        this.api = new Api();
    }

    componentWillMount() {
        this.setState({ strings: Strings[Strings.current] }, () => {});
        this.laodImages();
    }

    componentWillReceiveProps() {
        this.setState({ strings: Strings[Strings.current] }, () => {});
        this.laodImages();
    }

    laodImages() {
        this.api.loadImages(data => {
            if (data.code === 200 && data.data.images.length > 0) {
                this.fillImages(data.data);
            } else {
                this.setState({ main: this.error() }, () => {});
            }
        });
    }

    fillImages(data) {
        this.setState({
            images: data.images.map((image, i) => this.image(image, i)),
            indicators: data.images.map((a, i) => this.indicator(i))
        }, () => {
            this.setState({ main: this.carusetl() }, () => {});
        });
    }

    image(p, i) {
        return React.createElement(
            "div",
            { className: "carousel-item   " + (i === 0 ? "active" : 0), key: i },
            React.createElement(
                "div",
                { className: "index-calusel-item" },
                React.createElement("img", { className: "d-block index-img ", src: p.src, alt: "First slide", href: p.link })
            )
        );
    }

    indicator(index) {
        return React.createElement("li", { key: "indicators-" + index, "data-target": "#carouselExampleIndicators", "data-slide-to": index, className: !index ? "active" : "" });
    }

    carusetl() {
        return React.createElement(
            "div",
            { id: "carouselExampleControls", className: "index-carouser carousel slide", "data-ride": "carousel", style: { position: "absolute" } },
            React.createElement(
                "ol",
                { className: "carousel-indicators" },
                this.state.indicators
            ),
            React.createElement(
                "div",
                { className: "carousel-inner" },
                this.state.images
            ),
            React.createElement(
                "a",
                { className: "carousel-control-prev", href: "#carouselExampleControls", role: "button", "data-slide": "prev" },
                React.createElement("span", { className: "carousel-control-prev-icon", "aria-hidden": "true" }),
                React.createElement(
                    "span",
                    { className: "sr-only" },
                    "Previous"
                )
            ),
            React.createElement(
                "a",
                { className: "carousel-control-next", href: "#carouselExampleControls", role: "button", "data-slide": "next" },
                React.createElement("span", { className: "carousel-control-next-icon", "aria-hidden": "true" }),
                React.createElement(
                    "span",
                    { className: "sr-only" },
                    "Next"
                )
            )
        );
    }

    error() {
        return React.createElement(
            "div",
            { className: "alert alert-info text-center text-danger" },
            this.state.strings.index.images.error
        );
    }

    render() {
        return React.createElement(
            "div",
            null,
            this.state.main
        );
    }
}
//# sourceMappingURL=index.js.map
