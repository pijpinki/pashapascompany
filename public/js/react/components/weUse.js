class WeUseMain extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            cards: [],
            title: ""
        };
    }

    componentWillMount() {
        this.fillData();
    }

    componentWillReceiveProps() {
        this.fillData();
    }

    fillData() {
        let data = [];

        let card = [];

        let keys = Object.keys(Strings[Strings.current].weUse);

        keys.forEach(key => {
            if (key !== 'title') card.push(Strings[Strings.current].weUse[key]);
        });

        card.forEach((c, i) => {
            card[i]._technologies = c.technologies.map((t, j) => this.technology(t, j));
        });

        this.setState({ cards: card.map((c, i) => this.card(c, i)), title: Strings[Strings.current].weUse.title }, () => {});
    }

    technology(properties, i) {
        return React.createElement(
            "div",
            { className: "row", key: "row-" + i },
            React.createElement(
                "div",
                { className: "col-3" },
                React.createElement(
                    "h4",
                    { className: "lead" },
                    properties.title
                )
            ),
            React.createElement(
                "div",
                { className: "col-9" },
                React.createElement(
                    "p",
                    { className: "lead" },
                    properties.description
                )
            )
        );
    }

    card(properties, i) {
        return React.createElement(
            "div",
            { className: "jumbotron", key: "info" + i },
            React.createElement(
                "h1",
                { className: "display-3" },
                properties.title
            ),
            React.createElement(
                "div",
                { className: "" },
                properties._technologies
            )
        );
    }

    render() {
        return React.createElement(
            "div",
            null,
            React.createElement(
                "h1",
                { className: "display-3" },
                this.state.title
            ),
            React.createElement(
                "div",
                { className: "mt-5" },
                this.state.cards
            )
        );
    }
}
//# sourceMappingURL=weUse.js.map
