class PortfolioMain extends React.Component {
    constructor(p) {
        super(p);

        this.state = {
            products: []
        };

        this.count = 0;
        this.page = 0;
        this.api = new Api();
    }

    componentWillMount() {
        this.getProducts();
    }

    componentWillReceiveProps() {
        this.getProducts();
    }

    getProducts() {
        this.api.loadlProducts({ page: this.page }, data => {
            if (data.code === 200 && data.data.products.length > 0) {
                this.setState({ products: this.fillProducts(data.data.products) }, () => {});
            } else {
                this.setState({ products: this.error() }, () => {});
            }
        });
    }

    error() {
        return React.createElement(
            "div",
            { className: "alert alert-info" },
            React.createElement(
                "p",
                { className: "text-danger" },
                Strings[Strings.current].portfolio.products.error
            )
        );
    }

    fillProducts(products) {
        let count = 0;
        let selectedRow = 0;
        let _products = [];
        let rows = [[]];
        let final = [];

        _products = products.map(product => this.card(product));

        for (let i = 0; i < _products.length; i++) {
            if (count === 3) {
                rows.push([]);selectedRow++;count = 0;
            }

            rows[selectedRow].push(_products[i]);

            count++;
        }

        rows.forEach((row, i) => {
            final.push(React.createElement(
                "div",
                { className: "row mt-5", key: "row-" + i },
                row
            ));
        });

        return final;
    }

    /**
     * Generate products
     * @param product
     * @returns {XML}
     */
    card(product) {
        return React.createElement(
            "div",
            { className: "col-md", key: "product=" + product.id },
            React.createElement(
                "div",
                { className: "card ", style: { width: '100%' } },
                React.createElement(
                    "a",
                    { href: product.href },
                    React.createElement("img", { className: "card-img-top", src: product.image_src, alt: "Card image cap" })
                ),
                React.createElement(
                    "div",
                    { className: "card-body" },
                    React.createElement(
                        "h4",
                        { className: "card-title" },
                        product.title
                    ),
                    React.createElement(
                        "p",
                        { className: "card-text" },
                        product.description[Strings.current]
                    )
                )
            )
        );
    }

    render() {
        return React.createElement(
            "div",
            null,
            this.state.products
        );
    }
}
//# sourceMappingURL=portfolio.js.map
