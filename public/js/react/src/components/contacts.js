class ContactsMain extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            strings : {},
            descriptionInput : "",
            emailError : "",
            textError  : "",

            // Blocks
            form     : null,
            contacts : null,

            // Selected values
            selectedWho : 0,
            selectedEmail : "",
            selectedText  : ""

        };
    }

    componentWillMount(){
        this.fillData();
        this.setState({descriptionInput: Strings[Strings.current].contacts.form.descriptionClient}, ()=> {});
    }

    componentWillReceiveProps(){
        this.fillData();
        this.setState({descriptionInput: Strings[Strings.current].contacts.form.descriptionClient}, ()=> {});
    }

    fillData(){
        this.setState({strings : Strings[Strings.current].contacts}, () => {
            // generate form
        });
    }
    

    
    contact(parameters){
        return (
            <div className="col-lg-4">
                <img className="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140" />
                <h2>{parameters.title}</h2>
                <p>{parameters.info}</p>
            </div>
        );
    }

    render(){
        return (
            <div>
                <h1 className="display-3">{this.state.strings.title}</h1>
                <ContactsForm />
                <h1 className="display-4 mt-5">{this.state.strings.title2}</h1>
                <ContactsContcts />
            </div>
        )
    }
}

class ContactsForm extends React.Component {
    constructor(p){
        super(p);
        
        this.state = {
            strings : {},
            
            selectedText  : "",
            selectedEmail : "",
            selectedWho   : "",

            descriptionInput : "",
            emailError : "",
            textError  : "",
            
            select : null
        };
    }
    
    componentWillMount(){
        this.applyStrings();
    }

    componentWillReceiveProps(){
        this.applyStrings();
    }

    applyStrings(){
        this.setState({
            strings : Strings[Strings.current].contacts,
            descriptionInput : Strings[Strings.current].contacts.form.descriptionClient
        },this.makeSelect);
    }
    
    makeSelect(){
        this.setState({select : this.state.strings.form.clientSelect.map(c => this.select(c))},()=>{});
    }
    
    select(params){
        return (
            <option key={"select-"+params.id} value={params.id}>{params.title}</option>
        )
    }
    
    onChangeWhoAreYou(e){
        let val = parseInt(e.target.value);
        this.setState({selectedWho: val}, ()=>{});

        if(val === 0){
            this.setState({descriptionInput: this.state.strings.form.descriptionClient}, ()=>{});
        }else if (val === 1){
            this.setState({descriptionInput: this.state.strings.form.descriptionCompany}, ()=>{});
        }
    }
    
    onChangeEmail(e){
        let email = e.target.value;
        let erros = this.state.strings.form.errors.email;

        if(email.indexOf("@") === -1){
            this.setState({emailError : this.state.strings.form.errors.email.wrong, selectedEmail: ""},()=>{});   
        }else if(email.indexOf(".") === -1){
            this.setState({emailError : this.state.strings.form.errors.email.wrong, selectedEmail: ""},()=>{});
        }else {
            this.setState({selectedEmail : email, emailError: ""},()=>{});
        }

        if(email.indexOf("@") !== -1){
            emailValidation.call(this);
        }else if(email.indexOf(":") !== -1){
            skypeValidation.call(this);
        }else if(email.indexOf("+") !== -1){
            phoneValidation.call(this);
        }else {
            this.setState({emailError : erros.emailGlobal, selectedEmail: ""},()=>{});
        }

        function emailValidation() {
            if(email.indexOf(".") === -1){
                this.setState({emailError : erros.emailWrong, selectedEmail: ""},()=>{});
            }else {
                this.setState({emailError : "", selectedEmail : email},()=>{});
            }
        }

        function skypeValidation() {
            if(email.indexOf("skype") === -1){
                this.setState({emailError : erros.skypeWrong, selectedEmail : ""},()=>{});
            }else if(email.length < 9){
                this.setState({emailError : erros.skypeWrong, selectedEmail : ""},()=>{});
            }else {
                this.setState({emailError : "", selectedEmail : email},()=>{});
            }
        }

        function phoneValidation() {
            if(email.indexOf("+7") === -1){
                this.setState({emailError : erros.phoneGlobal, selectedEmail : ""},()=>{});
            }else if(email.length > 12){
                this.setState({emailError : erros.phoneBig, selectedEmail : ""},()=>{});
            }else if(email.length < 12){
                this.setState({emailError : erros.phoneShort, selectedEmail : ""},()=>{});
            }else {
                this.setState({emailError : "", selectedEmail : email},()=>{});
            }
        }
    }
    
    onChangeText(e){
        let text = e.target.value;
        
        if(text.length < 50){
            this.setState({textError : this.state.strings.form.errors.text.toShort, selectedText: ""},()=>{});
        }else {
            this.setState({selectedText : text, textError : ""},()=>{});
        }
    }

    send(){

    }
    
    render(){
        return (
            <form>
                <div className="form-group">
                    <label>{this.state.strings.form.whoAreYou}</label>
                    <select className="form-control" id="exampleFormControlSelect1" onChange={this.onChangeWhoAreYou.bind(this)}>{this.state.select}</select>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">{this.state.strings.form.youEmail}</label>
                    <input type="email" className="form-control" id="userEmail" aria-describedby="emailHelp" placeholder={this.state.strings.form.placeholders.email} onChange={this.onChangeEmail.bind(this)}/>
                    <label style={{display : this.state.emailError ? "block" : "none", color:"red"}}>{this.state.emailError}</label>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">{this.state.descriptionInput}</label>
                    <textarea rows="15" type="text" className="form-control" id="Input text" placeholder={this.state.strings.form.placeholders.text} onChange={this.onChangeText.bind(this)}/>
                    <label style={{display : this.state.textError ? "block" : "none", color:"red"}}>{this.state.textError}</label>
                </div>
                <button type="submit" className="btn btn-primary" onClick={this.send.bind(this)}>{this.state.strings.form.sendButton}</button>
            </form>
        )
    }
}

class ContactsContcts extends React.Component {
    constructor(p){
        super(p);
        
        this.state = {
            strings : {},
            contacts : null
        };
    }
    
    componentWillMount(){
        this.fillStrings();
    }

    componentWillReceiveProps(){
        this.fillStrings();
    }

    fillStrings(){
        let strings = Strings[Strings.current].contacts;
        this.setState({strings : strings}, this.fillConatacts.bind(this));
    }

    fillConatacts(){
        let contacts_keys = ['skype', 'email', 'phone'];
        let contects      = [];
        
        let keys = Object.keys(this.state.strings);
        
        contacts_keys.forEach(ck => {
            keys.forEach(k => {
               ck === k ? contects.push(this.state.strings[k]) : 0; 
            });
        });
        
        this.setState({contacts: contects.map(c => this.contact(c))},()=>{});
    }
    
    contact(parameters){
        return (
            <div className="col-md mt-5" key={parameters.id} style={{textAlign : "center"}}>
                <img className="rounded-circle images-size" src={parameters.icon} alt="Generic placeholder image" width="140" height="140" />
                <h2 className="">{parameters.title}</h2>
                <a className="text-primary" href={parameters.href}>{parameters.info}</a>
            </div>
        );
    }

    render(){
        return (<div className="row mt-5">{this.state.contacts}</div>)
    }
}