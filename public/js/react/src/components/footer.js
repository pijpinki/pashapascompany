class FooterMain extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            pages : []
        };

        this.pages = Config.footer;
    }

    componentWillMount(){
        this.fillPages();
    }

    componentWillReceiveProps(){
        this.fillPages();
    }

    fillPages(){
        let pages = [];
        for(let i = 0; i < this.pages.length; i++){
            pages.push(this.link(this.pages[i]));
        }

        this.setState({pages : pages}, ()=>{});
    }

    goToPage(page){
        window.location[page.type] = page.url;
    }

    link(page){
        return (
            <li className="nav-item" key={page.id}>
                <a className="nav-link" onClick={this.goToPage.bind(this, page)}>{page.text}</a>
            </li>
        );
    }

    render(){
        return(
            <nav className="navbar navbar-expand navbar-light bg-light mt-5">

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav m-auto">
                        {this.state.pages}
                    </ul>
                </div>
            </nav>
        )
    }
}