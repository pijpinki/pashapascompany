class PortfolioMain extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            products : []
        };

        this.count = 0;
        this.page  = 0;
        this.api   = new Api();
    }

    componentWillMount(){
        this.getProducts();
    }

    componentWillReceiveProps(){
        this.getProducts();
    }

    getProducts(){
        this.api.loadlProducts({page : this.page}, data =>{
            if(data.code === 200 && data.data.products.length > 0){
                this.setState({products : this.fillProducts(data.data.products)}, () => {});
            }else {
                this.setState({products : this.error()}, () => {});
            }
        })
    }

    error(){
        return (
            <div className="alert alert-info">
                <p className="text-danger">{Strings[Strings.current].portfolio.products.error}</p>
            </div>
        )
    }

    fillProducts(products){
        let count       = 0;
        let selectedRow = 0;
        let _products = [];
        let rows      = [[]];
        let final     = [];

        _products = products.map(product => this.card(product));

        for(let i = 0; i < _products.length; i++){
            if(count === 3) {rows.push([]); selectedRow++; count = 0;}

            rows[selectedRow].push(_products[i]);

            count++;
        }

        rows.forEach((row, i) => {
            final.push(<div className="row mt-5" key={"row-"+i}>{row}</div>);
        });


        return final;
    }

    /**
     * Generate products
     * @param product
     * @returns {XML}
     */
    card(product){
        return (
            <div className="col-md" key={"product="+product.id}>
                <div className="card " style={{width: '100%'}} >
                    <a href={product.href}><img className="card-img-top" src={product.image_src} alt="Card image cap"/></a>
                    <div className="card-body">
                        <h4 className="card-title">{product.title}</h4>
                        <p className="card-text">{product.description[Strings.current]}</p>
                    </div>
                </div>
            </div>
        );
    }

    render(){
        return (
            <div>
                {this.state.products}
            </div>
        );
    }
}
