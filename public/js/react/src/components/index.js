class IndexMain extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            images : null,
            title  : null,
            text   : null
        };

        this.main = p.main;
    }

    componentWillMount(){
        this.getImages();
        this.getText();
    }

    componentWillReceiveProps(){
        this.getImages();
        this.getText();
    }

    getImages(){
        this.setState({images : <IndexImages />}, ()=>{});
    }

    getText(){
        this.setState({title : Strings[Strings.current].index.body.title, text : this.generateDesc.call(this)}, () =>{});
    }

    generateDesc(){
        return Strings[Strings.current].index.body.description.map((text, index)=>this.desc(text, index));
    }

    desc(text, i){
        return(
            <p className="text-center lead" key={i}>{text}</p>
        )
    }

    render(){
        return (
            <div>
                {this.state.images}
                <div style={{marginTop:"100vh"}}>
                    <h1 className="text-center index-title display-2">{this.state.title}</h1>
                    {this.state.text}
                </div>
            </div>
        )
    }
}

class IndexImages extends React.Component {
    constructor(p){
        super(p);

        this.state={
            images  : [],
            strings : {},

            main       : null,
            indicators : null
        };

        this.api = new Api();
    }

    componentWillMount(){
        this.setState({strings: Strings[Strings.current]}, () =>{});
        this.laodImages();
    }

    componentWillReceiveProps(){
        this.setState({strings: Strings[Strings.current]}, () =>{});
        this.laodImages();
    }

    laodImages(){
        this.api.loadImages(data => {
            if(data.code === 200 && data.data.images.length > 0){
                this.fillImages(data.data);
            }else {
                this.setState({main : this.error()}, ()=>{});
            }
        });
    }

    fillImages(data){
        this.setState({
            images     : data.images.map((image ,i) => this.image(image,i)),
            indicators : data.images.map((a, i) => this.indicator(i))
        }, () =>{
            this.setState({main : this.carusetl()}, () => {});
        })
    }

    image(p,i){
        return(
            <div className={"carousel-item   "+(i === 0 ? "active" : 0)} key={i}>
                <div className="index-calusel-item">
                    <img className="d-block index-img " src={p.src} alt="First slide" href={p.link}/>
                </div>
            </div>
        );
    }

    indicator(index){
        return (
            <li key={"indicators-"+index} data-target="#carouselExampleIndicators" data-slide-to={index} className={!index ? "active" : ""} />
        )
    }

    carusetl(){
        return (
            <div id="carouselExampleControls" className="index-carouser carousel slide" data-ride="carousel" style={{position : "absolute"}}>
                <ol className="carousel-indicators">
                    {this.state.indicators}
                </ol>
                <div className="carousel-inner">
                    {this.state.images}
                </div>
                <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
        )
    }

    error(){
        return (
            <div className="alert alert-info text-center text-danger">{this.state.strings.index.images.error}</div>
        );
    }

    render(){
        return(
            <div>{this.state.main}</div>
        );
    }
}