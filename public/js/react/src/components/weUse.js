class WeUseMain extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            cards : [],
            title : ""
        };
    }

    componentWillMount(){
        this.fillData();
    }

    componentWillReceiveProps(){
        this.fillData();
    }

    fillData(){
        let data = [];

        let card = [];

        let keys = Object.keys(Strings[Strings.current].weUse);

        keys.forEach(key => {
            if(key !== 'title') card.push(Strings[Strings.current].weUse[key]);
        });

        card.forEach((c, i) => {
           card[i]._technologies =  c.technologies.map((t, j) => this.technology(t,j));
        });

        this.setState({cards: card.map((c, i) => this.card(c, i)), title : Strings[Strings.current].weUse.title}, ()=>{});
    }

    technology(properties, i){
        return (
            <div className="row" key={"row-"+i}>
                <div className="col-3"><h4 className="lead">{properties.title}</h4></div>
                <div className="col-9"><p className="lead">{properties.description}</p></div>
            </div>
        );
    }

    card(properties, i){
        return (
            <div className="jumbotron" key={"info"+i}>
                <h1 className="display-3">{properties.title}</h1>
                <div className="">{properties._technologies}</div>
            </div>
        );
    }

    render(){
        return (
            <div>
                <h1 className="display-3">{this.state.title}</h1>
                <div className="mt-5">{this.state.cards}</div>
            </div>
        )
    }
}