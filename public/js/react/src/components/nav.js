class NavMain extends React.Component {
    constructor(p){
        super(p);

        this.state = {
            pages : []
        };

        this.pages = Config.pages;

        this.current = p.current;
    }

    componentWillMount(){
        this.detectPage(1);
    }

    componentWillReceiveProps(){
        this.fillPages();
    }

    setHash(page){
        window.location.hash = page.url;
    }

    detectPage(current){
        $(window).on('hashchange', detect.bind(this));

        current ? detect.call(this) : 0;

        function detect() {
            let hash = window.location.hash;

            for(let i = 0; i < this.pages.length; i++){
                if(hash === this.pages[i].url){
                    this.setCurrentPage(this.pages[i]);
                    break;
                }
            }
        }
    }

    setCurrentPage(page){
        for(let i = 0; i < this.pages.length; i++){
            this.pages[i].active = 0;
        }

        this.pages[page.id].active = 1;

        this.fillPages();
    }

    fillPages(){
        let pages = [];

        for(let i = 0; i < this.pages.length; i++){
            pages.push(this.page(this.pages[i]));
        }

        this.setState({pages : pages}, () => {});
    }

    page(page){
        return (
            <li className={"nav-item "+ (page.active ? "active" : "")} key={page.id}>
                <a className="nav-link" href={page.url} onClick={this.setHash.bind(this, page)}>
                    {page.text}
                </a>
            </li>
        );
    }

    render(){
        return (
            <nav className="navbar-expand-sm navbar navbar-dark bg-primary">
                <a className="navbar-brand" href="#">
                    <img src="/images/icon.png" width="30" height="30" className="d-inline-block align-top" alt=""/>
                        Insane-IT
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <ul className="navbar-nav mr-auto">
                        {this.state.pages}
                    </ul>
                </div>
            </nav>
        )
    }

}