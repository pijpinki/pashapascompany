let Config = {
    api : "http://localhost:3000/api",
    pages : {
        0 : {id : 0, url : '#index',     active : 0, class : <IndexMain />,    text : Strings[Strings.current].nav.index},
        1 : {id : 1, url : '#portfolio', active : 0, class : <PortfolioMain />,text : Strings[Strings.current].nav.portfolio},
        2 : {id : 2, url : '#weuse',     active : 0, class : <WeUseMain />,    text : Strings[Strings.current].nav.weUse},
        3 : {id : 3, url : '#contacts',  active : 0, class : <ContactsMain />, text : Strings[Strings.current].nav.contacts},
        length : 4
    },

    footer : {
        0 : {id : 0, url : "#about",               active : 0, type: "hash", text : Strings[Strings.current].footer.about},
        1 : {id : 1, url : "https://bitbucket.org/pijpinki/pashapascompany", active : 0, type: "href", text : Strings[Strings.current].footer.src},
        length : 2
    }
};