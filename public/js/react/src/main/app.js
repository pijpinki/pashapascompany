class Main extends React.Component{
    constructor(){
        super();

        this.state = {
            page   : null
        };
    }

    componentWillMount(){
        this.detect();
        this.detectPage();
    }

    detect(){
        $(window).on('hashchange', this.detectPage.bind(this));
        !window.location.hash ? window.location.hash = Config.pages[0].url : 0;
    }

    detectPage(){
        for(let i = 0; i < Config.pages.length; i++){
            if(Config.pages[i].url === window.location.hash){
                this.setState({page : Config.pages[i].class}, () => {});
                break;
            }
        }
    }

    render(){
        return (<div className="container">{this.state.page}</div>);
    }
}

window.onload = function () {

    ReactDOM.render(
        <NavMain />,
        document.getElementById('nav')
    );

    ReactDOM.render(
        <Main />,
        document.getElementById('root')
    );

    ReactDOM.render(
        <FooterMain />,
        document.getElementById('footer')
    );
};
