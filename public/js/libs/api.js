class Api {
    constructor(){
        this.token = getCookie('token') || window.token || window.document.token || 'open';
        this.url   = Config.api;
    }

    loadImages(callback) {
        this.api({
            url     : this.url + "/images",
            method  : "get",
            data    : {},
            success : callback
        });
    }

    /**
     * Get products for portfolio
     * @param params
     * @param params.page
     * @param callback
     */
    loadlProducts(params, callback){
        this.api({
            url     : this.url + "/products",
            method  : "get",
            data    : params,
            success : callback
        });
    }

    /**
     *
     * @param params
     * @param params.url
     * @param params.method
     * @param params.data
     * @param params.success
     * @param callback
     */
    api(params,callback){
        $.ajax({
            url      : params.url,
            method   : params.method,
            data     : _plus({token : this.token},params.data),
            dataType : 'json',
            success  : params.success,
            error    : () => {setTimeout(this.api.bind(this, params, callback), 5000)}
        });
    }
}


let _plus = function (input, objct) {
    let out = {};

    let inoutKeys = Object.keys(input);
    let addKeys   = Object.keys(objct);

    inoutKeys.forEach(key => {
        out[key] = input[key];
    });

    addKeys.forEach(key => {
        out[key] = objct[key];
    });

    input = out;

    return out;
};

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}